package com.arun.aashu.popmenu_assignment;

import android.app.Dialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

public class MainActivity extends AppCompatActivity {
    EditText e1;
    String name,s,s2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

          s=((Button)findViewById(R.id.btn)).getText().toString();

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu p = new PopupMenu(MainActivity.this, findViewById(R.id.btn));
                p.getMenuInflater().inflate(R.menu.pop_menu, p.getMenu());


                p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {

                            case R.id.item1:
                                final Dialog dialogColour = new Dialog(MainActivity.this);


                                dialogColour.setContentView(R.layout.colour_custom_dialog);
                                dialogColour.setCancelable(true);
                                dialogColour.show();

                                dialogColour.findViewById(R.id.greenButton).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        findViewById(R.id.btn).setBackgroundColor(Color.GREEN);
                                        findViewById(R.id.btn).setBackgroundResource(R.drawable.green_corner);
                                        ((Button) findViewById(R.id.btn)).setTextColor(getResources().getColor(R.color.white));
                                        dialogColour.dismiss();
                                    }
                                });


                                dialogColour.findViewById(R.id.redButton).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        findViewById(R.id.btn).setBackgroundColor(Color.RED);
                                        findViewById(R.id.btn).setBackgroundResource(R.drawable.red_corner);
                                        ((Button) findViewById(R.id.btn)).setTextColor(getResources().getColor(R.color.white));
                                        dialogColour.dismiss();

                                    }
                                });


                                dialogColour.findViewById(R.id.pinkButton).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        findViewById(R.id.btn).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                        findViewById(R.id.btn).setBackgroundResource(R.drawable.pink_corner);
                                        ((Button) findViewById(R.id.btn)).setTextColor(getResources().getColor(R.color.white));
                                        dialogColour.dismiss();


                                    }
                                });

                                break;

                            case R.id.item2:
                                final Dialog dialogText = new Dialog(MainActivity.this);

                                dialogText.setContentView(R.layout.text_custom_dialog);
                                dialogText.setCancelable(true);
                                dialogText.show();

                                 e1 = dialogText.findViewById(R.id.edittext1);
                                e1.setText(s);


                                dialogText.findViewById(R.id.renameButton).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        name = e1.getText().toString();
                                        ((Button) findViewById(R.id.btn)).setText(name);
                                        dialogText.dismiss();

                                    }
                                });
                               s2= ((Button)findViewById(R.id.btn)).getText().toString();
                                e1.setText(s2);

                                break;

                        }

                        return false;
                    }
                });

                p.show();
            }
        });
    }
}
